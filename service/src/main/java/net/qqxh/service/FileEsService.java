package net.qqxh.service;

import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;

import java.io.IOException;
import java.util.List;

public interface FileEsService {
    boolean createFileIndex(SearchLib searchLib) throws IOException;
    String addFile2ES(String esIndex, String content, String name, String path, String viewFix) throws IOException;

    String deleteFileFromEs(String esIndex, String rid);

    List<Jfile> findFileByPath(SearchLib searchLib, String path);

    Jfile queryJfileByRid(SearchLib searchLib, String rid);
}
