package net.qqxh.service.task;

import net.qqxh.persistent.FileNode;
import net.qqxh.service.FileResolveService;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
public class FileReadTask {
    @Autowired
    FileResolveService fileResolveService;
    @Async("fileReadExecutor")
    public void readFile2FmartDate(FileNode fileNode, List<FileNode> tops, IOFileFilter fileFilter , FileReadTask fileReadTask,String root) {
        File file = new File(fileNode.getId());
        if (file.isDirectory()) {
            if(fileNode.getSons()==null){
                fileNode.setSons(new ArrayList<>());
            }
            File[] files = file.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    File f = files[i];
                    FileNode fn =new FileNode(f);
                    if (f.isDirectory()) {
                        if (StringUtils.equals(".svn", f.getName())) {
                            continue;
                        }
                        fileReadTask.readFile2FmartDate(fn,tops,fileFilter ,fileReadTask,root);
                        fileNode.getSons().add(fn);
                    } else {
                        if (f.getName().startsWith("~$")) {
                            continue;
                        }
                        if (fileFilter.accept(f)) {
                            fileNode.getSons().add(fn);

                        }
                    }
                    if(fn.getParentId().equals(root)){
                        tops.add(fn);
                    }
                }
            }

        } else {
            tops.add(fileNode);
        }
    }




}
