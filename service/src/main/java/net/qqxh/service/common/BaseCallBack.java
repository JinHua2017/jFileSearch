package net.qqxh.service.common;

import java.util.Map;

public interface BaseCallBack {
    public void doCallBack(Map<String, String> msg);
}
