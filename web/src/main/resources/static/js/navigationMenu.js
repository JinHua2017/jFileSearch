;(function ($, window, document, page, undefined) {
    var NavigationMenu = function (ele, options) {
        this.$element = ele,
            this.defaults = {
                multiple: false,
                menuList: [],
                defaut_target: "body",
                base_url:""
            },
            this.options = $.extend({}, this.defaults, options)
    };
    NavigationMenu.prototype = {
        init: function () {
            this.$element.addClass("accordion");

           /* history.redirect(null ,  this.options.base_url);*/
            page.base(this.options.base_url);
            this.fmt(this.$element, this.options.menuList, "")
            page({hashbang: true});
        },
        fmt: function (parent, menuList, p_router_path) {
            var _inner_multiple = this.options.multiple;
            var defaut_target = this.options.defaut_target;
            var _element = this.$element;
            var is_root = _element === parent;
            for (var i = 0; i < menuList.length; i++) {
                var menu = menuList[i];
                (function (menu, parent, fmt, defaut_target) {
                    var i_router_path = p_router_path + "_" + menu.router;
                    var _li = $("<li/>").append(function () {
                       var $this =$(this);
                            page(i_router_path, function () {

                                $this.parent("ul").show();
                                $this.parent("ul").parent("li").addClass("open")
                                $this.addClass("selected");
                                $.get(menu.url, function (r) {
                                    $(menu.target ? menu.target : defaut_target).html(r);
                                }, "html");
                            });
                            if (menu.children || is_root) {
                                return $("<div />").addClass("link").append(
                                    $("<i/>").addClass(menu.fa_icon)).append(menu.name).append(function () {
                                        if (menu.children) {
                                            return $("<i/>").addClass("fa fa-chevron-right")
                                        } else {
                                            return '';
                                        }
                                    }
                                ).on('click', {
                                    _children: menu.children,
                                    multiple: _inner_multiple || false
                                }, function (e) {
                                    var $this = $(this),
                                        $next = $this.next();
                                    if(e.data._children){
                                        $this.parent().toggleClass('selected');
                                    }

                                    $(".accordion").find('li').not($this.parent()).removeClass('selected');
                                    if (e.data._children) {
                                        /*触发该顶级的下级元素显示隐藏*/
                                        $next.slideToggle();
                                        $this.parent().toggleClass('open');
                                        /*解决ie8下字体不能重新渲染的问题*/
                                        var DEFAULT_VERSION = 8.0;
                                        var ua = navigator.userAgent.toLowerCase();
                                        var isIE = ua.indexOf("msie") > -1;
                                        var safariVersion;
                                        if (isIE) {
                                            safariVersion = ua.match(/msie ([\d.]+)/)[1];
                                        }
                                        if (safariVersion <= DEFAULT_VERSION) {
                                            var head = document.getElementsByTagName('head')[0],
                                                style = document.createElement('style');
                                            style.type = 'text/css';
                                            style.styleSheet.cssText = ':before,:after{content:none !important';
                                            head.appendChild(style);
                                            setTimeout(function () {
                                                head.removeChild(style);
                                            }, 0);
                                        }
                                        ;
                                        /*解决ie8下字体不能重新渲染的问题*/

                                        if (!e.data.multiple) {
                                            /*所有的其他的菜单隐藏，并修改父级样式为非打开模式*/
                                            $this.parent().parent().find('.submenu').not($next).slideUp().parent().removeClass('open');

                                        }
                                        ;
                                    } else {
                                        /*顶级连接*/
                                        page(i_router_path);
                                    }
                                })
                            } else {

                                return $("<a/>").attr("href", menu.url).text(menu.name).on('click', {
                                    router: i_router_path
                                }, function (e) {
                                    /*$(this).parent().toggleClass('selected');*/
                                    $(".accordion").find('li').not($(this).parent()).removeClass('selected');
                                    page(e.data.router);
                                    return false;
                                })
                            }
                        }
                    );

                    parent.append(_li);
                    if (menu.children) {
                        var next_ul = $("<ul/>").addClass("submenu");
                        if (menu.open === true) {
                            next_ul.css("display", "block");
                            _li.addClass("open");
                        }
                        _li.append(next_ul);
                        fmt(next_ul, menu.children, i_router_path);
                    }
                })(menu, parent, this.fmt, defaut_target)

            }
            return parent;
        }
    }
    $.fn.navigationMenu = function (options) {
        var navigationMenu = new NavigationMenu(this, options);
        return navigationMenu.init();
    }
})(jQuery, window, document, page);
