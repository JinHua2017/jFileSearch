var fileManagerApp = function (option) {
    this.$url = "fileManagerApp.html";
    this.$window = null;
}
fileManagerApp.prototype = {

    init: function () {
        this.$window = new Window(2, 800, 600, "文件管理");
        this.$window.initialize();
        this.$windowContent = $(this.$window.windowContent);
        this.$table = null;
        var _this = this;
        $.get(this.$url, function (r) {
            $(_this.$windowContent).html(r);
            _this.m2v();

        }, "html");


    },
    m2v: function () {
        var _this = this;

        var operateEvents = {
            "click #btn_view": function (e, value, row, index) {
                window.open('/showbypath?path=' + encodeURIComponent(row.id), '_balnk');
            },
            /*"click #btn_resolve": function (e, value, row, index) {
                _this.doAjax('/resolvebypath?path=' + encodeURIComponent(row.id));
            },*/
            "click #btn_update": function (e, value, row, index) {
                _this.doAjax('/svn/update?path=' + encodeURIComponent(row.id),function () {
                    _this.doAjax('/resolvebypath?path=' + encodeURIComponent(row.id));
                });


            }
        }
        this.$windowContent.find('.tree_table').bootstrapTable({
            class: 'table table-hover table-bordered',
            url: '/readFileList',
            height: 500,
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            sidePagination: 'server',
            pagination: false,
            treeView: true,
            treeId: "id",
            treeField: "name",
            columns: [{
                checkbox: true
            }, {
                field: 'name',
                title: '名称',
            },
                {
                    field: 'text',
                    title: '操作',
                    formatter: _this.operateFormatter,
                    events: operateEvents
                }
            ]
        });
        this.$table = this.$windowContent.find('.tree_table');
        this.$windowContent.find(".expandAllTree").on('click', function () {
            _this.$table.bootstrapTable("expandAllTree")
            $(this).hide();
            _this.$windowContent.find(".collapseAllTree").show();
        });
        this.$windowContent.find(".collapseAllTree").on('click', function () {
            _this.$table.bootstrapTable("collapseAllTree")
            $(this).hide();
            _this.$windowContent.find(".expandAllTree").show();
        })
    },
    operateFormatter: function (value, row, index) {
        if (row.isParent != true) {
            return [
               /* '<button id="btn_resolve" type="button " class="btn btn-outline-secondary btn-sm">转换</button>',*/
                '<button id="btn_view" type="button " class="btn_view btn btn-outline-primary btn-sm">查看</button>',
                '<button id="btn_update" type="button " class="btn_update btn btn-outline-success btn-sm">更新</button>'
            ].join('');
        } else {
            return [
                /*'<button id="btn_resolve" type="button " class="btn btn-outline-secondary btn-sm">转换</button>',*/
                '<button id="btn_update" type="button " class="btn_update btn btn-outline-success btn-sm">更新</button>'
            ].join('');
        }

    }
    ,
    doAjax: function (url,callback) {
        {
            $.ajax({
                url: url,
                type: 'get',
                data: {},
                cache: false,
                success: function (res) {
                    resResolve(res);
                    if(typeof callback =="function"){
                        callback();
                    }

                },
                error: function (res) {
                    resResolve(res);
                }
            });
        }
    }

}