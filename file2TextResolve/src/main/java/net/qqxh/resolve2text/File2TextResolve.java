package net.qqxh.resolve2text;

import java.io.File;
import java.io.IOException;

public interface File2TextResolve {
    String resolve(File data) throws IOException;
    String getType();
}
